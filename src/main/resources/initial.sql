INSERT INTO role (id, name) VALUES ('1', 'ROLE_ADMIN');
INSERT INTO role (id, name) VALUES ('2', 'ROLE_ALUNO');
INSERT INTO role (id, name) VALUES ('3', 'ROLE_PARCEIRO_PF');
INSERT INTO role (id, name) VALUES ('4', 'ROLE_COMERCIAL');
INSERT INTO role (id, name) VALUES ('5', 'ROLE_MARKETING');
INSERT INTO role (id, name) VALUES ('6', 'ROLE_PERSONAL');
INSERT INTO role (id, name) VALUES ('7', 'ROLE_PERSONAL_PJ');
INSERT INTO role (id, name) VALUES ('8', 'ROLE_ASSINANTE');

INSERT INTO usuario (email, password) VALUES ('joao.emilio@gmail.com', '1234');
INSERT INTO usuario (email, password) VALUES ('renatocsare@gmail.com', '1234');
INSERT INTO usuario (email, password) VALUES ('kauebentes@gmail.com', '1234');

INSERT INTO usuario_roles (usuario_email, roles_id) values ('joao.emilio@gmail.com', '1');
INSERT INTO usuario_roles (usuario_email, roles_id) values ('kauebentes@gmail.com', '1');
INSERT INTO usuario_roles (usuario_email, roles_id) values ('renatocsare@gmail.com', '1');
