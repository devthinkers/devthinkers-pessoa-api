drop table IF EXISTS  produto_imagens;
drop table IF EXISTS  produto;
drop table IF EXISTS  categoria_produto;

drop table IF EXISTS personal_trainer_servico;
drop table IF EXISTS personal_trainer_cliente;
drop table IF EXISTS personal_trainer_bairro;
drop table IF EXISTS personal_trainer_academia;
drop table IF EXISTS personal_trainer;

drop table IF EXISTS  cliente_serie;
drop table IF EXISTS  cliente;

drop table IF EXISTS  grupos_musculares;

drop table IF EXISTS serie_exercicios;
drop table IF EXISTS exercicio;
drop table IF EXISTS serie;

drop table IF EXISTS  imagem;

drop table IF EXISTS  noticia;

drop table IF EXISTS  parceiro;

drop table IF EXISTS  promocao_imagens;
drop table IF EXISTS  promocao;

drop table IF EXISTS  usuario_roles;
drop table IF EXISTS  role_users;
drop table IF EXISTS  role;
drop table IF EXISTS  usuario;

drop table IF EXISTS entidade_imagens;
drop table IF EXISTS  entidade;

drop table IF EXISTS  servico;
drop table IF EXISTS  telefone;
drop table IF EXISTS  email;

drop table IF EXISTS  bairro;
drop table IF EXISTS  cidade;
drop table IF EXISTS  estado;
drop table IF EXISTS  pais;
drop table IF EXISTS  endereco;
