package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Telefone;
import com.devthinkers.api.pessoa.repository.TelefoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TelefoneService {
	
	@Autowired

	private TelefoneRepository repository;
	
	public Telefone add(Telefone telefone) {
		telefone = repository.save(telefone);
		return telefone;
	}
	
	public Telefone get(String id) {
		return repository.findOne(id);
	}

	public void update(Telefone telefone) {
		repository.save(telefone);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Telefone> findAll() {
		return repository.findAll();
	}

}
