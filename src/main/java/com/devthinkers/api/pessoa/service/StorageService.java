package com.devthinkers.api.pessoa.service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Service
public class StorageService {
    private static final Logger log = LoggerFactory.getLogger(StorageService.class);

    @Value("${aws_namecard_bucket}")
    private String bucketName;

    @Autowired
    private AmazonS3Client amazonS3Client;

    public String uploadFile(MultipartFile file, String imagemId ) {

        AmazonS3 s3client = new AmazonS3Client(  );
        try {
            System.out.println("Uploading a new object to S3 from a file\n");

            File convFile = new File(imagemId); // new File( "c:/tmp/" + file.getOriginalFilename());
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType( file.getContentType()  );
            objectMetadata.setContentLength( file.getBytes().length );

            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, imagemId , file.getInputStream(), objectMetadata);
            putObjectRequest.setCannedAcl(CannedAccessControlList.PublicRead);

            PutObjectResult putObjectResult = amazonS3Client.putObject(putObjectRequest);
        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which " +
                    "means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which " +
                    "means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
