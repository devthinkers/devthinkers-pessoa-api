package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Cliente;
import com.devthinkers.api.pessoa.domain.UDA;
import com.devthinkers.api.pessoa.repository.ClienteRepository;
import com.devthinkers.api.pessoa.repository.UDARepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository repository;

	@Autowired
	private UDARepository udasRepository;

	public Cliente add(Cliente cliente) {
		//cliente.setEndereco(enderecoService.persist(cliente.getEndereco()));
		cliente = repository.save(cliente);
		return cliente;
	}
	
	public Cliente get(String id) {
		return repository.findOne(id);
	}

	public void update(Cliente cliente) {
		repository.save(cliente);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Cliente> findAll() {
		return repository.findAll();
	}

	public List<UDA> findUDAsByCliente( Cliente cliente ) {
		return this.udasRepository.findUDAsByClienteId( cliente.getId() );
	}


	public UDA findUDA(String id, String chave) {
		UDA.PK pk = new UDA.PK();
		pk.setChave(chave);
		pk.setClienteId(id);
		UDA uda = this.udasRepository.findOne( pk );
		return uda;
	}

	public UDA saveUDA(UDA uda) {
		return this.udasRepository.save( uda );
	}
}
