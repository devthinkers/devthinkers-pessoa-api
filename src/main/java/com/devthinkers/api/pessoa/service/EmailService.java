package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Email;
import com.devthinkers.api.pessoa.repository.EmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailService {
	
	@Autowired
	private EmailRepository repository;
	
	public Email add(Email email) {
		email = repository.save(email);
		return email;
	}
	
	public Email get(String id) {
		return repository.findOne(id);
	}

	public void update(Email email) {
		repository.save(email);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Email> findAll() {
		return repository.findAll();
	}

}
