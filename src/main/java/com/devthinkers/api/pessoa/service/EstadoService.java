package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Estado;
import com.devthinkers.api.pessoa.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstadoService {
	
	@Autowired
	private EstadoRepository repository;

	public Estado add(Estado estado) {
		estado = repository.save(estado);
		return estado;
	}

	public Estado findByUf(String uf) {
		return repository.findByUf(uf);
	}

	public Estado get(String id) { return repository.findOne(id);}

	public void update(Estado estado) {
		repository.save(estado);
	}

	public void delete(String id) { repository.delete(id); }

	public List<Estado> findAll() {
		return repository.findAll();
	}

}
