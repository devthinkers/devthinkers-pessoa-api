package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Cidade;
import com.devthinkers.api.pessoa.repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeService {
	
	@Autowired
	private CidadeRepository repository;

	public Cidade add(Cidade cidade) {
		cidade = repository.save(cidade);
		return cidade;
	}
	
	public Cidade get(String id) {
		return repository.findOne(id);
	}

	public void update(Cidade cidade) {
		repository.save(cidade);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Cidade> findAll() {
		return repository.findAll();
	}

    public Cidade findByNome(String nome) {
		return repository.findByNome(nome);
    }
}
