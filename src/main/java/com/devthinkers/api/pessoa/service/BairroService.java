package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Bairro;
import com.devthinkers.api.pessoa.repository.BairroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BairroService {
	
	@Autowired
	private BairroRepository repository;

	public Bairro add(Bairro bairro) {
		bairro = repository.save(bairro);
		return bairro;
	}
	
	public Bairro get(String id) {
		return repository.findOne(id);
	}

	public void update(Bairro bairro) {
		repository.save(bairro);
	}

	public void delete(String id) {
		repository.delete(id);
	}

	public List<Bairro> findAll() {
		return repository.findAll();
	}

    public Bairro findByNome(String nome) { return repository.findByNome(nome);}
}
