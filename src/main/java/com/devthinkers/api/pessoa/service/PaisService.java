package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.Pais;
import com.devthinkers.api.pessoa.repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class PaisService {
	
	@Autowired
	private PaisRepository repository;

	public Pais add(Pais pais) {
		pais = repository.save(pais);
		return pais;
	}

	public Pais findByNome(String nome) {
		return repository.findByNome(nome);
	}
	
	public Pais get(String id) {
		return repository.findOne(id);
	}

	public void update(Pais pais) {
		repository.save(pais);
	}

	public void delete(String id) { repository.delete(id); }

	public List<Pais> findAll() {
		return repository.findAll();
	}

}
