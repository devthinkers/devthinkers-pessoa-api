package com.devthinkers.api.pessoa.service;

import com.devthinkers.api.pessoa.domain.*;
import com.devthinkers.api.pessoa.repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoService {
	
	@Autowired
	private EnderecoRepository repository;

	@Autowired
	private PaisService paisService;

	@Autowired
	private EstadoService estadoService;

	@Autowired
	private CidadeService cidadeService;

	@Autowired
	private BairroService bairroService;

	private Endereco endereco;

	public Endereco add(Endereco endereco) {
		endereco = repository.save(endereco);
		return endereco;
	}

	public Endereco get(String id) { return repository.findOne(id);}

	public void update(Endereco endereco) {
		repository.save(endereco);
	}

	public void delete(String id) { repository.delete(id); }

	public List<Endereco> findAll() {
		return repository.findAll();
	}

	private Endereco getEndereco(Endereco endereco) {

		this.endereco = endereco;

		//verifica na tabela se já existe nome correspondente
		Pais pais = paisService.findByNome(endereco.getPais().getNome());
		Estado estado = estadoService.findByUf(endereco.getEstado().getUf());
		Cidade cidade = cidadeService.findByNome(endereco.getCidade().getNome());
		Bairro bairro = bairroService.findByNome(endereco.getBairro().getNome());

		//verifica se é um novo objeto
		savePais(pais);
		saveEstado(estado);
		saveCidade(cidade);
		saveBairro(bairro);

		return endereco;
	}

	public Endereco persist(Endereco endereco) {
		endereco = getEndereco(endereco);
		return repository.save(endereco);
	}

	private void savePais(Pais pais) {
		if(pais == null) {
			pais = new Pais();
			pais.setNome(endereco.getPais().getNome());
			pais = paisService.add(pais);
		}
		endereco.setPais(pais);
	}

	private void saveEstado(Estado estado) {
		if(estado == null) {
			estado = new Estado();
			estado.setUf(endereco.getEstado().getUf());
			estado.setPais(endereco.getPais());
			estado = estadoService.add(estado);
		}
		endereco.setEstado(estado);
	}

	private void saveCidade(Cidade cidade) {
		if(cidade == null) {
			cidade = new Cidade();
			cidade.setNome(endereco.getCidade().getNome());
			cidade.setEstado(endereco.getEstado());
			cidade = cidadeService.add(cidade);
		}
		endereco.setCidade(cidade);
	}

	private void saveBairro(Bairro bairro) {
		if (bairro == null) {
			bairro = new Bairro();
			bairro.setNome(endereco.getBairro().getNome());
			bairro.setCidade(endereco.getCidade());
			bairro = bairroService.add(bairro);
		}
		endereco.setBairro(bairro);
	}
}
