package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Cliente;
import com.devthinkers.api.pessoa.domain.UDA;
import com.devthinkers.api.pessoa.service.ClienteService;
import com.devthinkers.api.pessoa.service.EnderecoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author renatocesar
 * 04-28-2017
 *
 */

@RestController
@RequestMapping("/api/pessoa")
public class ClienteController {

	private static final Logger log = LoggerFactory.getLogger(ClienteController.class);

	@Autowired
	private ClienteService service;

	@Autowired
	private EnderecoService enderecoService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Cliente> add(@RequestBody Cliente cliente) {
		log.debug("Adicionando Cliente: " + cliente);
		cliente = service.add(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Cliente> get(@PathVariable("id") String id) {
		log.debug("Recuperando Cliente: " + id);
		Cliente cliente = service.get(id);
		if( cliente != null ) {
			return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
		} else {
			return new ResponseEntity<Cliente>(cliente, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Cliente> update(@RequestBody Cliente cliente) {
		log.debug("Atualizando Cliente: " + cliente.getId());
		service.update(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Cliente> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Cliente: " + id);
		Cliente cliente = service.get(id);
		if( cliente != null ) {
			service.delete(id);
			return new ResponseEntity<Cliente>(cliente, HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<Cliente>(cliente, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public List<Cliente> findAll() {
		log.debug("FindAll Cliente");
		List<Cliente> clientes = service.findAll();
		return clientes;
	}

	@RequestMapping(value="/{id}/uda", method = RequestMethod.GET)
	public ResponseEntity<List<UDA>> getUDAs(@PathVariable("id") String id) {
		log.debug("Recuperando UDAs: " + id);
		Cliente cliente = new Cliente();
		cliente.setId(id);
		List<UDA> udas = service.findUDAsByCliente( cliente );
		System.out.println( udas );
		return new ResponseEntity<List<UDA>>(udas, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}/uda/{chave}", method = RequestMethod.GET)
	public UDA getUDA(@PathVariable("id") String id, @PathVariable("chave") String chave) {
		log.debug("Recuperando UDAs: " + id + " " + chave );
		UDA uda = service.findUDA( id , chave );
		System.out.println( uda );
		return uda;
	}

	@RequestMapping(value="/{id}/uda/{chave}", method = RequestMethod.POST)
	public UDA saveUDA(@RequestBody UDA uda, @PathVariable("id") String id, @PathVariable("chave") String chave) {


		log.debug("Recuperando UDAs: " + id + " " + chave );

		UDA.PK pk = new UDA.PK();
		pk.setClienteId( id );
		pk.setChave( chave );
		uda.setPk(pk);

		uda = service.saveUDA( uda );
		System.out.println( uda );
		return uda;
	}

}
