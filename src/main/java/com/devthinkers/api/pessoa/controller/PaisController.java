package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Pais;
import com.devthinkers.api.pessoa.service.PaisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author renatocesar
 * 04-28-2017
 */

@RestController
@RequestMapping("/api/pais")
public class PaisController {

	private static final Logger log = LoggerFactory.getLogger(PaisController.class);

	@Autowired
	private PaisService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Pais> add(@RequestBody Pais pais) {
		log.debug("Adicionando Pais: " + pais);
		pais = service.add(pais);
		return new ResponseEntity<Pais>(pais, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Pais> get(@PathVariable("id") String id) {
		log.debug("Recuperando Pais: " + id);
		Pais pais = service.get(id);
		return new ResponseEntity<Pais>(pais, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Pais> update(@RequestBody Pais pais) {
		log.debug("Atualizando Pais: " + pais.getCodigo());
		service.update(pais);
		return new ResponseEntity<Pais>(pais, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Pais> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Pais: " + id);
		service.delete(id);
		return new ResponseEntity<Pais>(HttpStatus.OK);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Pais> findAll() {
		log.debug("FindAll Pais");
    	List<Pais> pais = service.findAll();
        return pais;
    }
}
