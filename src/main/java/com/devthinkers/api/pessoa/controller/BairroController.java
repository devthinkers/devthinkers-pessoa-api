package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Bairro;
import com.devthinkers.api.pessoa.service.BairroService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author renatocesar
 * 05-18-2017
 */

@RestController
@RequestMapping("/api/bairro")
public class BairroController {

	private static final Logger log = LoggerFactory.getLogger(BairroController.class);

	@Autowired
	private BairroService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Bairro> add(@RequestBody Bairro bairro) {
		log.debug("Adicionando Bairro: " + bairro);
		bairro = service.add(bairro);
		return new ResponseEntity<Bairro>(bairro, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Bairro> get(@PathVariable("id") String id) {
		log.debug("Recuperando Bairro: " + id);
		Bairro bairro = service.get(id);
		return new ResponseEntity<Bairro>(bairro, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Bairro> update(@RequestBody Bairro bairro) {
		log.debug("Atualizando Bairro: " + bairro.getId());
		service.update(bairro);
		return new ResponseEntity<Bairro>(bairro, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Bairro> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Bairro: " + id);
		service.delete(id);
		return new ResponseEntity<Bairro>(HttpStatus.OK);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Bairro> findAll() {
		log.debug("FindAll Bairro");
    	List<Bairro> bairro = service.findAll();
        return bairro;
    }
}
