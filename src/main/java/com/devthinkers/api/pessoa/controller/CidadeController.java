package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Cidade;
import com.devthinkers.api.pessoa.service.CidadeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author renatocesar
 * 04-28-2017
 */

@RestController
@RequestMapping("/api/cidade")
public class CidadeController {

	private static final Logger log = LoggerFactory.getLogger(CidadeController.class);

	@Autowired
	private CidadeService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Cidade> add(@RequestBody Cidade cidade) {
		log.debug("Adicionando Cidade: " + cidade);
		cidade = service.add(cidade);
		return new ResponseEntity<Cidade>(cidade, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Cidade> get(@PathVariable("id") String id) {
		log.debug("Recuperando Cidade: " + id);
		Cidade cidade = service.get(id);
		return new ResponseEntity<Cidade>(cidade, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Cidade> update(@RequestBody Cidade cidade) {
		log.debug("Atualizando Cidade: " + cidade.getId());
		service.update(cidade);
		return new ResponseEntity<Cidade>(cidade, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Cidade> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Cidade: " + id);
		service.delete(id);
		return new ResponseEntity<Cidade>(HttpStatus.OK);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Cidade> findAll() {
		log.debug("FindAll Cidade");
    	List<Cidade> cidade = service.findAll();
        return cidade;
    }
}
