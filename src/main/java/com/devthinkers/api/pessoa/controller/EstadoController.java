package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Estado;
import com.devthinkers.api.pessoa.service.EstadoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author renatocesar
 * 04-28-2017
 */

@RestController
@RequestMapping("/api/estado")
public class EstadoController {

	private static final Logger log = LoggerFactory.getLogger(EstadoController.class);

	@Autowired
	private EstadoService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Estado> add(@RequestBody Estado estado) {
		log.debug("Adicionando Estado: " + estado);
		estado = service.add(estado);
		return new ResponseEntity<Estado>(estado, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Estado> get(@PathVariable("id") String id) {
		log.debug("Recuperando Estado: " + id);
		Estado estado = service.get(id);
		return new ResponseEntity<Estado>(estado, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Estado> update(@RequestBody Estado estado) {
		log.debug("Atualizando Estado: " + estado.getUf());
		service.update(estado);
		return new ResponseEntity<Estado>(estado, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Estado> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Estado: " + id);
		service.delete(id);
		return new ResponseEntity<Estado>(HttpStatus.OK);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Estado> findAll() {
		log.debug("FindAll Estado");
    	List<Estado> estado = service.findAll();
        return estado;
    }
}
