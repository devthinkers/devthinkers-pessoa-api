package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Email;
import com.devthinkers.api.pessoa.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author renatocesar
 * 04-28-2017
 */

@RestController
@RequestMapping("/api/email")
public class EmailController {

	private static final Logger log = LoggerFactory.getLogger(EmailController.class);

	@Autowired
	private EmailService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Email> add(@RequestBody Email email) {
		log.debug("Adicionando Email: " + email);
		email = service.add(email);
		return new ResponseEntity<Email>(email, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Email> get(@PathVariable("id") String id) {
		log.debug("Recuperando Email: " + id);
		Email email = service.get(id);
		return new ResponseEntity<Email>(email, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Email> update(@RequestBody Email email) {
		log.debug("Atualizando Email: " + email.getId());
		service.update(email);
		return new ResponseEntity<Email>(email, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Email> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Email: " + id);
		service.delete(id);
		return new ResponseEntity<Email>(HttpStatus.OK);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Email> findAll() {
		log.debug("FindAll Email");
    	List<Email> emails = service.findAll();
        return emails;
    }
}
