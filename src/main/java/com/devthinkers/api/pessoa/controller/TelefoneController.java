package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Telefone;
import com.devthinkers.api.pessoa.service.TelefoneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author renatocesar
 * 04-28-2017
 */

@RestController
@RequestMapping("/api/telefone")
public class TelefoneController {

	private static final Logger log = LoggerFactory.getLogger(TelefoneController.class);

	@Autowired
	private TelefoneService service;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Telefone> add(@RequestBody Telefone telefone) {
		log.debug("Adicionando Telefone: " + telefone);
		telefone = service.add(telefone);
		return new ResponseEntity<Telefone>(telefone, HttpStatus.CREATED);
	}

	@RequestMapping(value="{id}", method = RequestMethod.GET)
	public ResponseEntity<Telefone> get(@PathVariable("id") String id) {
		log.debug("Recuperando Telefone: " + id);
		Telefone telefone = service.get(id);
		return new ResponseEntity<Telefone>(telefone, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Telefone> update(@RequestBody Telefone telefone) {
		log.debug("Atualizando Telefone: " + telefone.getId());
		service.update(telefone);
		return new ResponseEntity<Telefone>(telefone, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Telefone> delete(@PathVariable("id") String id) {
		System.out.println("Excluindo Telefone: " + id);
		service.delete(id);
		return new ResponseEntity<Telefone>(HttpStatus.OK);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Telefone> findAll() {
		log.debug("FindAll Telefone");
    	List<Telefone> telefones = service.findAll();
        return telefones;
    }
}
