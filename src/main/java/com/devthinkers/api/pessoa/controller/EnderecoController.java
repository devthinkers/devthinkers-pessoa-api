package com.devthinkers.api.pessoa.controller;

import com.devthinkers.api.pessoa.domain.Endereco;
import com.devthinkers.api.pessoa.service.EnderecoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by renat on 26/05/2017.
 */

@RestController
@RequestMapping("/api/endereco")
public class EnderecoController {

    private static final Logger log = LoggerFactory.getLogger(EnderecoController.class);

    @Autowired
    private EnderecoService service;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Endereco> add(@RequestBody Endereco endedereco) {
        log.debug("Adicionando Endereco: " + endedereco);
        endedereco = service.add(endedereco);
        return new ResponseEntity<Endereco>(endedereco, HttpStatus.CREATED);
    }

    @RequestMapping(value="{id}", method = RequestMethod.GET)
    public ResponseEntity<Endereco> get(@PathVariable("id") String id) {
        log.debug("Recuperando Endereco: " + id);
        Endereco endedereco = service.get(id);
        return new ResponseEntity<Endereco>(endedereco, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Endereco> update(@RequestBody Endereco endedereco) {
        log.debug("Atualizando Endereco: " + endedereco.getId());
        service.update(endedereco);
        return new ResponseEntity<Endereco>(endedereco, HttpStatus.OK);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Endereco> delete(@PathVariable("id") String id) {
        System.out.println("Excluindo Endereco: " + id);
        service.delete(id);
        return new ResponseEntity<Endereco>(HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Endereco> findAll() {
        log.debug("FindAll Endereco");
        List<Endereco> enderecos = service.findAll();
        return enderecos;
    }




}
