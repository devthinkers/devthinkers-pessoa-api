package com.devthinkers.api.pessoa.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by joaoe on 03/07/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AWSIDToken {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
