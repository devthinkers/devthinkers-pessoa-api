package com.devthinkers.api.pessoa.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Estado {

    @Id
    private String uf;

    @OneToOne
    private Pais pais;

    public String getUf() { return uf; }

    public void setUf(String uf) { this.uf = uf; }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
}