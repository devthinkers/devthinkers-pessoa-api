package com.devthinkers.api.pessoa.domain;

/**
 * Created by joaoemilio on 08/05/2017.
 */
public enum TipoTelefone {

    COMERCIAL, CELULAR, RESIDENCIAL, CONTATO, OUTRO

}
