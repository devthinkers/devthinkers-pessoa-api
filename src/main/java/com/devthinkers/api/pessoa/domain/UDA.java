package com.devthinkers.api.pessoa.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class UDA implements Serializable {

    @Embeddable
    public static class PK implements Serializable {
        private String clienteId;
        private String chave;

        public String getClienteId() {
            return clienteId;
        }

        public void setClienteId(String clienteId) {
            this.clienteId = clienteId;
        }

        public String getChave() {
            return chave;
        }

        public void setChave(String chave) {
            this.chave = chave;
        }
    }

    @EmbeddedId
    private PK pk;

    private String valor;

    public PK getPk() {
        return pk;
    }

    public void setPk(PK pk) {
        this.pk = pk;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
