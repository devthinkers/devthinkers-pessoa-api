package com.devthinkers.api.pessoa.domain;

/**
 * Created by renat on 26/05/2017.
 */
public enum TipoEmail {

    PESSOAL, COMERCIAL
}
