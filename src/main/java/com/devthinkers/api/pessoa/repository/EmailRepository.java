package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Email;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailRepository extends CrudRepository<Email, String> {

    List<Email> findAll();


}