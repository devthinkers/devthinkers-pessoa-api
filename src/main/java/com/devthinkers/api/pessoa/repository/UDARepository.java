package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.UDA;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UDARepository extends CrudRepository<UDA, UDA.PK> {

    List<UDA> findAll();

    @Query("SELECT u FROM UDA u where u.pk.clienteId = :clienteId")
    List<UDA> findUDAsByClienteId(@Param("clienteId") String clienteId);
}