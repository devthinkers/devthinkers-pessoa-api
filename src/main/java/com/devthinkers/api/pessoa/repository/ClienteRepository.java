package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, String> {
	
	List<Cliente> findAll();

}
