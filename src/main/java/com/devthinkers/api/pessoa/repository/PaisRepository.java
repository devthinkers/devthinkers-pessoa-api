package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Pais;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaisRepository extends CrudRepository<Pais, String> {

    List<Pais> findAll();

    Pais findByNome(String nome);
}