package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Telefone;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TelefoneRepository extends CrudRepository<Telefone, String> {

    List<Telefone> findAll();


}