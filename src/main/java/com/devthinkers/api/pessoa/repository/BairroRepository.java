package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Bairro;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BairroRepository extends CrudRepository<Bairro, String> {

    List<Bairro> findAll();

    Bairro findByNome(String nome);
}