package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Estado;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadoRepository extends CrudRepository<Estado, String> {

    List<Estado> findAll();

    Estado findByUf(String uf);
}