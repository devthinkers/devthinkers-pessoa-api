package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Cidade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadeRepository extends CrudRepository<Cidade, String> {

    List<Cidade> findAll();

    Cidade findByNome(String nome);
}