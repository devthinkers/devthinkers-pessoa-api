package com.devthinkers.api.pessoa.repository;

import com.devthinkers.api.pessoa.domain.Endereco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<Endereco, String> {

    List<Endereco> findAll();

}